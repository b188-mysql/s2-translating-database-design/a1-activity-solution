CREATE DATABASE blog_db;

-- users table
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

-- posts table
CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(500),
	content VARCHAR(5000) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	author_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_author_id
		FOREIGN KEY (author_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- post_comments table
CREATE TABLE post_comments (
	id INT NOT NULL AUTO_INCREMENT,
	content VARCHAR(5000) NOT NULL,
	datetime_commented DATETIME NOT NULL,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_comments_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- post_likes table
CREATE TABLE post_likes (
	id INT NOT NULL AUTO_INCREMENT,
	datetime_liked DATETIME NOT NULL,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_likes_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


